<p align="center"><img width=80% src="media/logoFont.png"></p>

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# NPRG024 - Interpreter Design Pattern
Following repository contains an implementation project for `NPRG024` course. Project demonstrates the usage of classic `Interpreter` design pattern in `Java` programming language. The CLI application performs basic `Reverse Polish Notation` calculations.

### Structure
Project is generated using gradle CLI Applicaiton template. The source codes are inside the `src/main` repository. Solution also has a simple `AppTest.java` class that implements 3 units tests checking the main function with 3 different reverse polish notations.

### Usage
From withing the global directory, execute the following command to start the CLI app:

`gradle run` - To execute the application. Console will wait for user input, and output the results of calculations line by line. Enter empty line to stop input loop.

`gradle test` - To execute unit tests with pre defined reverse polish notations.

`gradle javadoc` - To generate javadoc and explore the class hierarchies and structure.

### Supported operations

```
• Multiplication - Left Expression * Right Expression
• Division - Left Expression / Right Expression
• Addition - Left Expression + Right Expression
• Subtraction - Left Expression - Right Expression
• Modulo of - Left Expression % Right Expression
• Root of - √(Left Expression)
```

### Sample input patterns
---

Input : `√100 6 %` 
Should Produce: `4.0`

---

Input : `100 2 + 12 4 / * ` 
Should Produce: `306.0`

---

Input : `√100 6 % 123 289 * / √625 +` 
Should Produce: `25.000112527076826`

---
