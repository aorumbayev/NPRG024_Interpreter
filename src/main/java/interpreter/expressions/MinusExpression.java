package interpreter.expressions;

/**
 * Interprets Minus Expression
 */
public class MinusExpression extends Expression {

  private Expression leftExpression;
  private Expression rightExpression;

  public MinusExpression(Expression leftExpression, Expression rightExpression) {
    this.leftExpression = leftExpression;
    this.rightExpression = rightExpression;
  }

  @Override
  public double interpret() {
    return (leftExpression.interpret() - rightExpression.interpret());
  }

  @Override
  public String toString() {
    return "-";
  }

}
