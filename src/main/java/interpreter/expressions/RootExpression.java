package interpreter.expressions;

/**
 * Interprets Root Expression
 */
public class RootExpression extends Expression {

    private Expression expression;

    public RootExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public double interpret() {
        return Math.sqrt(expression.interpret());
    }

    @Override
    public String toString() {
        return "√";
    }
}
