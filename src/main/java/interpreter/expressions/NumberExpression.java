package interpreter.expressions;

/**
 * Interprets Number expression
 */
public class NumberExpression extends Expression {

  private double number;

  public NumberExpression(double number) {
    this.number = number;
  }

  public NumberExpression(String s) {
    this.number = Double.parseDouble(s);
  }

  @Override
  public double interpret() {
    return number;
  }

  @Override
  public String toString() {
    return "number";
  }
}
