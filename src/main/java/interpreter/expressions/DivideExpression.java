package interpreter.expressions;

/**
 * Interprets Divide Expression
 */
public class DivideExpression extends Expression {
    private Expression leftExpression;
    private Expression rightExpression;

    public DivideExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public double interpret() {
        return (leftExpression.interpret() / rightExpression.interpret());
    }

    @Override
    public String toString() {
        return "/";
    }
}
