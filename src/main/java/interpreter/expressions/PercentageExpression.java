package interpreter.expressions;

/**
 * Interprets the `modulo of` operation
 */
public class PercentageExpression extends Expression {

    private Expression leftExpression;
    private Expression rightExpression;

    public PercentageExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public double interpret() {
        double leftValue = leftExpression.interpret();
        double rightValue = rightExpression.interpret();
        return leftValue % rightValue;
    }

    @Override
    public String toString() {
        return "%";
    }
}
