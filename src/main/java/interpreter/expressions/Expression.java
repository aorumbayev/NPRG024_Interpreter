package interpreter.expressions;


public abstract class Expression {

  /**
   * @return Abstract method declaration, each subclass will implement
   * required logic within this method to handle the expression
   */
  public abstract double interpret();

  @Override
  public abstract String toString();
}
