package interpreter;

import interpreter.expressions.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {

  public static void main(String[] args) {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String line = "";

    try {
      while (!(line = br.readLine()).trim().equals("")) {
        System.out.println(performReverseComputation(line));
      }
    } catch(IOException e){
      e.printStackTrace();
    }
  }

  /**
   * @param tokenString Takes the input string splits it by whitespace and performs
   *                    reverse polish notation's calculations
   * @return  Should return the result of reverse polish calculation
   */
  public static double performReverseComputation(String tokenString) {

    Stack<Expression> expStack = new Stack<>();

    String[] tokenList = tokenString.split(" ");
    for (String s : tokenList) {
      if (isOperator(s)) {
        Expression rightExpression = expStack.pop();
        Expression leftExpression = expStack.pop();

        Expression operator = getOperatorInstance(s, leftExpression, rightExpression);

        double result = operator.interpret();
        NumberExpression resultExpression = new NumberExpression(result);
        expStack.push(resultExpression);
      } else {
        Expression i = getNumberInstance(s);
        expStack.push(i);
      }
    }

    if  (expStack.size() > 1) {
      System.out.println("WARNING: notation is incomplete, returning " +
              "first element from the stack!");
    }

    return expStack.empty() ? 0 : expStack.pop().interpret();
  }

  /**
   * @param rawOperator Takes the raw string and checks whether it is an operator
   * @return True if it is an operator, false otherwise
   */
  public static boolean isOperator(String rawOperator) {
    return rawOperator.equals("+") ||
            rawOperator.equals("-") ||
            rawOperator.equals("*") ||
            rawOperator.equals("/") ||
            rawOperator.equals("") ||
            rawOperator.equals("%");
  }

  /**
   * @param rawInput
   * @param left Represents left hand side expression class
   * @param right Represents right hand side expression class
   * @return Returns resulting expression
   */
  public static Expression getOperatorInstance(String rawInput, Expression left, Expression right) {
    switch (rawInput) {
      case "+":
        return new PlusExpression(left, right);
      case "-":
        return new MinusExpression(left, right);
      case "*":
        return new MultiplyExpression(left, right);
      case "/":
        return new DivideExpression(left, right);
      case "%":
        return new PercentageExpression(left, right);
      case "√":
        return new RootExpression(right);
      default:
        return new MultiplyExpression(left, right);
    }
  }


  /**
   * Represent Regex patterns for identifying root number and simple digit number
   */
  private final static String ROOT_REGEX = "√((\\d)+)";
  private final static String DIGIT_REGEX = "^(\\d)+$";
  private final static Pattern rootPattern = Pattern.compile(ROOT_REGEX, Pattern.MULTILINE);
  private final static Pattern digitPattern = Pattern.compile(DIGIT_REGEX, Pattern.MULTILINE);


  /**
   * @param rawInput
   * @return Returns Parsed Number expression based on content of raw string
   */
  public static Expression getNumberInstance(String rawInput) {

    if (rawInput.isEmpty()) return new NumberExpression(0.0);

    Matcher rootMatcher = rootPattern.matcher(rawInput);
    Matcher digitMatcher = digitPattern.matcher(rawInput);

    if (rootMatcher.matches()) {
      return new RootExpression(new NumberExpression(rootMatcher.group(1)));
    } else if (digitMatcher.matches()) {
      return new NumberExpression(digitMatcher.group(0));
    } else return new NumberExpression(0.0);

  }

}
